using System;
using Harmony;
using System.Reflection;
using UnityEngine;
using DMT;
using System.IO;
using System.Collections.Generic;

namespace RH_StashBackpack
{
    // TODO : 
    // Vehicle storage
    // Ignore supply satchel things - DONE
    // Save

    public class RH_StashBackpack_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_BackpackWindow))]
    [HarmonyPatch("BtnSort_OnPress")]
    [HarmonyPatch(new Type[] { typeof(XUiController), typeof(OnPressEventArgs) })]
    public class PatchXUiC_BackpackWindowBtnSort_OnPress
    {
        static bool Prefix(XUiC_BackpackWindow __instance, ref XUiController _sender, ref OnPressEventArgs _e)
        {
            ItemStack itemStack = null;
            if (__instance.xui.AssembleItem.CurrentItemStackController != null)
            {
                itemStack = __instance.xui.AssembleItem.CurrentItemStackController.ItemStack;
            }
            __instance.xui.PlayerInventory.SortStacks(__instance.xui);
            if (itemStack != null)
            {
                __instance.GetChildByType<XUiC_ItemStackGrid>().AssembleLockSingleStack(itemStack);
            }

            return false;
        }
    }


    [HarmonyPatch(typeof(XUiM_PlayerInventory))]
    [HarmonyPatch("SortStacks")]
    [HarmonyPatch(new Type[] { typeof(XUi) })]
    public class PatchXUiM_PlayerInventorySortStacks
    {
        static bool Prefix(XUiM_PlayerInventory __instance, ref XUi _xui)
        {
            ItemStack[] array = __instance.GetBackpackItemStacks();
            ItemStack[] arrayLockItems = new ItemStack[array.Length];
            var _XUiC_BackpackWindow = (XUiC_BackpackWindow)_xui.FindWindowGroupByName("backpack").GetChildById("windowBackpack");
            XUiC_ItemStackGrid childByType = _XUiC_BackpackWindow.GetChildByType<XUiC_ItemStackGrid>(); 
            for (int i = 0; i <= array.Length - 1; i++)
            {
                // NOTE : The last item in array[] is not set within the outer loop, instead its set within the inner loops! This means we need to manually set the last item in our cloned array arrayLockItems
                // changed 'i < array.Length - 1' to 'i <= array.Length - 1' to load in the last item in the array but break out once arrayLockItems[i] is set

                var xuiC_ItemStack = (XUiC_ItemStack)childByType.itemControllers[i];
                if (xuiC_ItemStack.IsDumpLocked)
                {
                    arrayLockItems[i] = array[i];
                    array[i] = ItemStack.Empty.Clone();

                    if (i == array.Length - 1)
                        break;
                }
                else
                {
                    arrayLockItems[i] = ItemStack.Empty.Clone();
                    if (i == array.Length - 1)
                        break;

                    if (array[i].IsEmpty())
                    {
                        for (int j = i + 1; j < array.Length; j++)
                        {
                            var xuiC_ItemStack2 = (XUiC_ItemStack)childByType.itemControllers[j];
                            if (!array[j].IsEmpty() && !xuiC_ItemStack2.IsDumpLocked)
                            {
                                array[i] = array[j];
                                array[j] = ItemStack.Empty.Clone();
                                break;
                            }
                        }
                    }
                    if (!array[i].IsEmpty())
                    {
                        ItemClass itemClass = array[i].itemValue.ItemClass;
                        int num = itemClass.Stacknumber.Value - array[i].count;
                        if (!itemClass.HasQuality && num != 0)
                        {
                            for (int k = i + 1; k < array.Length; k++)
                            {
                                var xuiC_ItemStack2 = (XUiC_ItemStack)childByType.itemControllers[k];
                                if (xuiC_ItemStack2.IsDumpLocked)
                                    continue;

                                if (array[i].itemValue.type == array[k].itemValue.type)
                                {
                                    int num2 = Utils.FastMin(array[k].count, num);
                                    array[i].count += num2;
                                    array[k].count -= num2;
                                    num -= num2;
                                    if (array[k].count == 0)
                                    {
                                        array[k] = ItemStack.Empty.Clone();
                                    }
                                }
                                if (num == 0)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            array = StackSortUtil.SortStacks(array);
            int arrayId = 0;
            for (int i = 0; i <= arrayLockItems.Length - 1 ; i++)
            {
                if (arrayLockItems[i].IsEmpty())
                {
                    arrayLockItems[i] = array[arrayId];
                    arrayId++;
                }
            }

            __instance.backpack.SetSlots(arrayLockItems);

            return false;
        }
    }

    [HarmonyPatch(typeof(XUiC_ItemStack))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    public class PatchXUiC_ItemStackUpdate
    {
        static bool Prefix(XUiC_ItemStack __instance, ref float _dt)
        {
            LocalPlayerUI playerUI = __instance.xui.playerUI;
            PlayerActionsGUI guiactions = playerUI.playerInput.GUIActions;
            UICamera uiCamera = playerUI.uiCamera;

            if (__instance.WindowGroup.isShowing)
            {
                if (!__instance.bLocked && !__instance.isDragAndDrop)
                {
                    if (__instance.isOver)
                    {
                        if (Input.GetKey(KeyCode.X))
                        {
                            if (__instance.NextDumpTick == 0 || GameTimer.Instance.ticks >= __instance.NextDumpTick)
                            {
                                __instance.IsDumpLocked = !__instance.IsDumpLocked;
                                __instance.NextDumpTick = GameTimer.Instance.ticks + 5;
                                UpdateBagLockedSlots(__instance.xui.PlayerInventory.GetBackpackItemStacks(), __instance.xui);
                            }
                        }
                    }
                }
            }
         
            return true;
        }

        static void Postfix(XUiC_ItemStack __instance, ref float _dt)
        {
            if (__instance.WindowGroup.isShowing)
            {
                if (!__instance.isOver && __instance.IsDumpLocked)
                    __instance.background.Color = new Color(0.960f, 0.513f, 0.478f);
            }
        }

        private static void UpdateBagLockedSlots(ItemStack[] backpackItemStacks, XUi _xui)
        {
            EntityPlayer entityPlayer = GameManager.Instance.World.GetPrimaryPlayer();
            var _XUiC_BackpackWindow = (XUiC_BackpackWindow)_xui.FindWindowGroupByName("backpack").GetChildById("windowBackpack");
            XUiC_ItemStackGrid childByType = _XUiC_BackpackWindow.GetChildByType<XUiC_ItemStackGrid>();

            for (int i = 0; i < backpackItemStacks.Length; i++)
            {
                var xuiC_ItemStack = (XUiC_ItemStack)childByType.itemControllers[i];
                if (xuiC_ItemStack != null)
                    entityPlayer.BagLockedSlots[i] = xuiC_ItemStack.IsDumpLocked;
            }
        }
    }

    [HarmonyPatch(typeof(PlayerDataFile))]
    [HarmonyPatch("Read")]
    [HarmonyPatch(new Type[] { typeof(BinaryReader), typeof(uint) })]
    public class PatchPlayerDataFileRead
    {
        static void Postfix(PlayerDataFile __instance, BinaryReader _br, uint _version)
        {
            //Log.Out("PatchPlayerDataFileRead 11 ");
            var lockedSlots = new List<bool>();
            for (int i = 0; i < 60; i++)
            {
                lockedSlots.Add(_br.ReadBoolean());
            }
            __instance.BagLockedSlots = lockedSlots;
            //Log.Out("PatchPlayerDataFileRead 22 : " + __instance.BagLockedSlots.Count);
        }
    }

    [HarmonyPatch(typeof(PlayerDataFile))]
    [HarmonyPatch("Write")]
    [HarmonyPatch(new Type[] { typeof(BinaryWriter) })]
    public class PatchPlayerDataFileWrite
    {
        static void Postfix(PlayerDataFile __instance, BinaryWriter _bw)
        {
            //Log.Out("PatchPlayerDataFileWrite 11 ");

            if (__instance.BagLockedSlots == null)
            {
                var bagSlots = new List<bool>();
                for(int i = 0; i < 60; i++)
                {
                    bagSlots.Add(false);
                }
                __instance.BagLockedSlots = bagSlots;
            }

            foreach (var locked in __instance.BagLockedSlots)
            {
                _bw.Write(locked);
            }
            //Log.Out("PatchPlayerDataFileWrite 22 : " + __instance.BagLockedSlots.Count);
        }
    }

    [HarmonyPatch(typeof(PlayerDataFile))]
    [HarmonyPatch("ToPlayer")]
    [HarmonyPatch(new Type[] { typeof(EntityPlayer) })]
    public class PatchPlayerDataFileToPlayer
    {
        static void Postfix(PlayerDataFile __instance, EntityPlayer _player)
        {
            //Log.Out("PatchPlayerDataFileToPlayer 11 ");
            // Copy from saved PlayerDataFile to BagLockedSlots
            _player.BagLockedSlots = __instance.BagLockedSlots;
            //Log.Out("PatchPlayerDataFileToPlayer 22 ");
        }
    }

    [HarmonyPatch(typeof(PlayerDataFile))]
    [HarmonyPatch("FromPlayer")]
    [HarmonyPatch(new Type[] { typeof(EntityPlayer) })]
    public class PatchPlayerDataFileFromPlayer
    {
        static void Postfix(PlayerDataFile __instance, EntityPlayer _player)
        {
            //Log.Out("PatchPlayerDataFileFromPlayer 11 ");
            if (_player.BagLockedSlots == null)
            {
                var bagSlots = new List<bool>();
                for (int i = 0; i < 60; i++)
                {
                    bagSlots.Add(false);
                }
                _player.BagLockedSlots = bagSlots;
            }
            
            __instance.BagLockedSlots = _player.BagLockedSlots;
            //Log.Out("PatchPlayerDataFileFromPlayer 22 ");
        }
    }
}
