﻿using System;
using Harmony;
using System.Reflection;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Linq;
using UnityEngine;
using DMT;

public class RH_BiomeSpawnScaling
{
    public class RH_BiomeSpawnScaling_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    private static void LoadBiomeGamestages()
    {
        if (!BiomeSpawningClass.GamestagesParsed)
        {
            foreach (var key in BiomeSpawningClass.list.Dict.Keys)
            {
                var biomeSpawnEntityGroupList = BiomeSpawningClass.list[key];
                foreach (var biomeSpawnEntityGroupData in biomeSpawnEntityGroupList.list)
                {
                    var gs = 1;
                    if (biomeSpawnEntityGroupData.entityGroupRefName.Contains("-")
                        && biomeSpawnEntityGroupData.entityGroupRefName.Split('-').Length > 1
                        && int.TryParse(biomeSpawnEntityGroupData.entityGroupRefName.Split('-')[1], out gs))
                    {
                        biomeSpawnEntityGroupData.gameStage = gs;
                    }
                    else
                        biomeSpawnEntityGroupData.gameStage = gs;
                }
            }

            BiomeSpawningClass.GamestagesParsed = true;
        }
    }

    [HarmonyPatch(typeof(ChunkAreaBiomeSpawnData))]
    [HarmonyPatch("IsSpawnNeeded")]
    [HarmonyPatch(new Type[] { typeof(WorldBiomes), typeof(ulong) })]
    public class PatchChunkAreaBiomeSpawnDataIsSpawnNeeded
    {
        static bool Prefix(ChunkAreaBiomeSpawnData __instance, ref bool __result, ref WorldBiomes _worldBiomes, ref ulong _worldTime)
        {
            if (DateTime.Now.Second == 0 && GameManager.Instance.LastLoggedMinute != DateTime.Now.Minute)
            {
                GameManager.Instance.LastLoggedMinute = DateTime.Now.Minute;

                var biomeEnemyCount = 0;
                var num = 0;
                for (int i = GameManager.Instance.World.Entities.list.Count - 1; i >= 0; i--)
                {
                    Entity entity = GameManager.Instance.World.Entities.list[i];
                    if (entity.IsDead())
                        continue;

                    EntityAlive entityAlive = null;
                    if (entity is EntityAlive)
                    {
                        entityAlive = (EntityAlive)entity;
                    }

                    if(!entity.IsDead() && entityAlive != null && entityAlive.GetSpawnerSource() == EnumSpawnerSource.Biome)
                    {
                        if (EntityClass.list[entity.entityClass] != null && EntityClass.list[entity.entityClass].bIsEnemyEntity)
                            biomeEnemyCount++;
                    }

                    //Log.Out(string.Concat(new object[]
                    //{
                    //    ++num,
                    //    ". id=",
                    //    entity.entityId,
                    //    ", ",
                    //    entity.ToString(),
                    //     ", spawnSource=",
                    //    (entityAlive != null) ? (entityAlive.GetSpawnerSource().ToString()) : "",
                    //    ", pos=",
                    //    entity.GetPosition().ToCultureInvariantString(),
                    //    ", rot=",
                    //    entity.rotation.ToCultureInvariantString(),
                    //    ", lifetime=",
                    //    (entity.lifetime == float.MaxValue) ? "float.Max" : entity.lifetime.ToCultureInvariantString("0.0"),
                    //    ", remote=",
                    //    entity.isEntityRemote.ToString(),
                    //    ", dead=",
                    //    entity.IsDead().ToString(),
                    //    ", ",
                    //    (entityAlive != null) ? ("health=" + entityAlive.Health) : ""
                    //}));
                }

                Log.Out("Number of enemies in game is " + GameStats.GetInt(EnumGameStats.EnemyCount) + " out of a maximum " + GetMaxZombies() + ". Biome enemy count = " + biomeEnemyCount);
            }

            LoadBiomeGamestages();

            BiomeDefinition biome = _worldBiomes.GetBiome(__instance.biomeId);
            if (biome == null)
            {
                __result = false;
                return false;
            }
            BiomeSpawnEntityGroupList biomeSpawnEntityGroupList = BiomeSpawningClass.list[biome.m_sBiomeName];
            if (biomeSpawnEntityGroupList == null)
            {
                __result = false;
                return false;
            }

            if (GameStats.GetInt(EnumGameStats.EnemyCount) >= (GetMaxZombies() * 0.9))
            {
                __result = false;
                return false;
            }

            if (GameManager.Instance.World.m_ChunkManager.ChunkPOIs == null)
                GameManager.Instance.World.m_ChunkManager.ChunkPOIs = new Dictionary<long, ulong>();

            if(!GameManager.Instance.World.m_ChunkManager.ChunkPOIs.Keys.Contains(__instance.chunk.Key))
            {
                var POIs = GetAllPrefabFromWorldPosInside(__instance.chunk);
                //Log.Out("Loading Chunk POI Count : " + __instance.chunk.Key + " - POI Count = " + POIs.Count());

                GameManager.Instance.World.m_ChunkManager.ChunkPOIs.Add(__instance.chunk.Key, (ulong)POIs.Count());
            }

            var partyLevel = 0;
            var list = GetBiomeSpawnEntityGroupData(biomeSpawnEntityGroupList.list, __instance.chunk, out partyLevel);

            for (int i = 0; i < list.Count; i++)
            {
                string key = list[i].entityGroupRefName + "_" + list[i].daytime.ToStringCached<EDaytime>();
                if (!__instance.entitesSpawned.ContainsKey(key) || __instance.entitesSpawned[key].count < GetPOISpawnCount(list[i].maxCount, GameManager.Instance.World.m_ChunkManager.ChunkPOIs[__instance.chunk.Key], __instance.chunk.Key) || __instance.entitesSpawned[key].respawnLockedUntilWorldTime <= _worldTime)
                {
                    EDaytime edaytime = (!GameManager.Instance.World.IsDaytime()) ? EDaytime.Night : EDaytime.Day;
                    if (list[i].daytime == EDaytime.Any || list[i].daytime == edaytime)
                    {
                        __result = true;
                        return false;
                    }
                }
            }
            __result = false;
            return false;
        }
    }

    [HarmonyPatch(typeof(SpawnManagerBiomes))]
    [HarmonyPatch("Spawn")]
    [HarmonyPatch(new Type[] { typeof(string), typeof(bool), typeof(ChunkAreaBiomeSpawnData) })]
    public class PatchSpawnManagerBiomesspawn
    {
        static bool Prefix(SpawnManagerBiomes __instance, ref string _spawnerName, ref bool _bSpawnEnemyEntities, ref ChunkAreaBiomeSpawnData _chunkBiomeSpawnData)
        {
            // Delay biome spawning until 10pm
            if (__instance.world.worldTime < 22000UL)
                return false;

            if (_chunkBiomeSpawnData == null)
            {
                return false;
            }

            // only spawn 90% of MaxSpawn to allow zombies to spawn in sleepers
            if (_bSpawnEnemyEntities && GameStats.GetInt(EnumGameStats.EnemyCount) >= (GetMaxZombies() * 0.9))
            {
                _bSpawnEnemyEntities = false;
            }
            if (_bSpawnEnemyEntities && __instance.world.aiDirector.BloodMoonComponent.BloodMoonActive)
            {
                _bSpawnEnemyEntities = false;
            }
            if (!_bSpawnEnemyEntities && GameStats.GetInt(EnumGameStats.AnimalCount) >= GamePrefs.GetInt(EnumGamePrefs.MaxSpawnedAnimals))
            {
                return false;
            }
            int minDistance = (!_bSpawnEnemyEntities) ? 48 : 28;
            int maxDistance = (!_bSpawnEnemyEntities) ? 70 : 54;
            Vector3 zero = Vector3.zero;
            bool flag = false;
            List<EntityPlayer> players = __instance.world.GetPlayers();
            for (int i = 0; i < players.Count; i++)
            {
                if (players[i].Spawned)
                {
                    Vector3 position = players[i].GetPosition();
                    Rect rect = new Rect(position.x - 40f, position.z - 40f, 80f, 80f);
                    if (rect.Overlaps(_chunkBiomeSpawnData.area))
                    {
                        flag = true;
                        break;
                    }
                }
            }
            if (!flag || !__instance.world.GetRandomSpawnPositionInAreaMinMaxToPlayers(_chunkBiomeSpawnData.area, minDistance, maxDistance, true, out zero))
            {
                return false;
            }
            BiomeDefinition biome = __instance.world.Biomes.GetBiome(_chunkBiomeSpawnData.biomeId);
            BiomeSpawnEntityGroupList biomeSpawnEntityGroupList;
            if (biome == null || (biomeSpawnEntityGroupList = BiomeSpawningClass.list[biome.m_sBiomeName]) == null)
            {
                return false;
            }

            var partyLevel = 0;
            var list = GetBiomeSpawnEntityGroupData(biomeSpawnEntityGroupList.list, _chunkBiomeSpawnData.chunk, out partyLevel);
            EDaytime edaytime = (!__instance.world.IsDaytime()) ? EDaytime.Night : EDaytime.Day;
            string text = null;
            int num = -1;
            int num2 = UnityEngine.Random.Range(0, list.Count);
            int j = 0;
            while (j < 5)
            {
                //Log.Out("SpawnManagerBiomes num2 : " + num2);
                if (list[num2].daytime == EDaytime.Any || list[num2].daytime == edaytime)
                {
                    bool flag2 = EntityGroups.IsEnemyGroup(list[num2].entityGroupRefName);
                    if (!flag2 || _bSpawnEnemyEntities)
                    {
                        int num3 = list[num2].maxCount;
                        //Log.Out("num3 AA : " + num3);
                        if (flag2)
                        {
                            num3 = EntitySpawner.ModifySpawnCountByGameDifficulty(num3);
                            //Log.Out("num3 BB : " + num3);
                        }

                        num3 = GetPOISpawnCount(num3, GameManager.Instance.World.m_ChunkManager.ChunkPOIs[_chunkBiomeSpawnData.chunk.Key], _chunkBiomeSpawnData.chunk.Key, true);
                        //Log.Out("num3 CC : " + num3);
                        //Log.Out(" ------------------ ");

                        text = list[num2].entityGroupRefName + "_" + list[num2].daytime.ToStringCached<EDaytime>();
                        ulong respawnLockedUntilWorldTime = _chunkBiomeSpawnData.GetRespawnLockedUntilWorldTime(text);
                        if (respawnLockedUntilWorldTime <= 0UL || __instance.world.worldTime >= respawnLockedUntilWorldTime)
                        {
                            if (respawnLockedUntilWorldTime > 0UL)
                            {
                                _chunkBiomeSpawnData.ClearRespawnLocked(text);
                            }
                            int entitiesSpawned = _chunkBiomeSpawnData.GetEntitiesSpawned(text);
                            if (entitiesSpawned < num3)
                            {
                                num = num2;
                                break;
                            }
                        }
                    }
                }
                j++;
                num2 = (num2 + 1) % list.Count;
            }
            if (text == null || num == -1)
            {
                return false;
            }
            int lastClassId = 0;
            int randomFromGroup = EntityGroups.GetRandomFromGroup(list[num].entityGroupRefName, ref lastClassId);
            float spawnDeadChance = list[num].spawnDeadChance;
            _chunkBiomeSpawnData.IncEntitiesSpawned(text);
            Entity entity = EntityFactory.CreateEntity(randomFromGroup, zero);
            entity.SetSpawnerSource(EnumSpawnerSource.Biome, _chunkBiomeSpawnData.chunk.Key, text);
            //Log.Out(string.Concat(new object[] { "Biome spawning : ", list[num].entityGroupRefName, " for partylevel :", partyLevel, " - randomFromGroup : ", randomFromGroup, " - entity : ", entity.EntityClass.entityClassName, " - chunkKey : ", _chunkBiomeSpawnData.chunk.Key }));
            __instance.world.SpawnEntityInWorld(entity);
            if (spawnDeadChance > 0f && UnityEngine.Random.value < spawnDeadChance)
            {
                entity.Kill(DamageResponse.New(true));
            }
            __instance.world.DebugAddSpawnedEntity(entity);

            return false;
        }
    }



    public static int GetGamestageFromBounds(int x, int y, int z, string source, bool log = false)
    {
        // Get Players gamestage
        return GetGamestageFromBounds((float)x, (float)y, (float)z, source, log);
    }

    public static int GetGamestageFromBounds(float x, float y, float z, string source, bool log = false)
    {
        //if (log)
        //    Log.Out("BiomeSpawning ---------");

        // Get Players gamestage
        var chunkPosition = new Vector3(x, y, z);
        var bounds = new Bounds(new Vector3(chunkPosition.x, chunkPosition.y, chunkPosition.z), new Vector3(200f, 200f, 200f));
        List<Entity> playersInBounds = GameManager.Instance.World.GetEntitiesInBounds(typeof(EntityPlayer), bounds, new List<Entity>());
        List<int> partyLevelList = new List<int>();
        foreach (var entityBound in playersInBounds)
        {
            var player = entityBound as EntityPlayer;
            partyLevelList.Add(player.gameStage);

            //if (log)
            //    Log.Out("BiomeSpawning Player in bound : " + player.entityId + " : GS = " + player.gameStage + " : source = " + source);
        }

        return Mathf.Max(GameStageDefinition.CalcPartyLevel(partyLevelList), 1);
    }

    public static List<PrefabInstance> GetAllPrefabFromWorldPosInside(Chunk _chunk)
    {
        var allPrefabs = GameManager.Instance.World.ChunkClusters[0].ChunkProvider.GetDynamicPrefabDecorator().allPrefabs;
        var prefabs = new List<PrefabInstance>();

        for (int i = 0; i < allPrefabs.Count; i++)
        {
            PrefabInstance prefabInstance = allPrefabs[i];
            if(Overlaps(prefabInstance,_chunk, 75f))
            {
                prefabs.Add(allPrefabs[i]);
            }
        }

        return prefabs;
    }

    public static bool Overlaps(PrefabInstance prefabInstance, Chunk _chunk, float _expandBounds = 0f)
    {
        Bounds aabb = _chunk.GetAABB();
        aabb.Expand(_expandBounds);
        Vector3 max = aabb.max;
        Vector3 min = aabb.min;
        Bounds aabb2 = prefabInstance.GetAABB();
        Vector3 max2 = aabb2.max;
        Vector3 min2 = aabb2.min;

        return min.x <= max2.x && max.x >= min2.x && min.y <= max2.y && max.y >= min2.y && min.z <= max2.z && max.z >= min2.z;
    }

    public static int GetPOISpawnCount(int maxCount, ulong poiCount, long chunkKey, bool log = false)
    {
        var POIZombieMultiplier = GamePrefs.GetInt(EnumGamePrefs.POIZombieMultiplier) > 3 ? 3 : GamePrefs.GetInt(EnumGamePrefs.POIZombieMultiplier);
        var count = maxCount + ((int)poiCount * POIZombieMultiplier);

        //if (log)
        //    Log.Out("GetPOISpawnCount 11 : " + chunkKey + " - " + maxCount + " + " + poiCount + " = " + count);

        var enemyCount = GameStats.GetInt(EnumGameStats.EnemyCount);
        var maxCountScalingPercent = 0f;
        if(enemyCount > 0)
            maxCountScalingPercent = (enemyCount >= GetMaxZombies()) ? 1f : ((float)enemyCount / (float)GetMaxZombies());

        //if (log)
        //    Log.Out("GetPOISpawnCount 22 : " + chunkKey + " - " + maxCountScalingPercent);

        // Maximum per chunk is 1/6th of max zombies. This is then scaled on percentage of max zombies already spawned in. 
        // So if max zombies is 120 thats a max of 20 per chunk. If 60 zombies are already spawned in then max is 50% of 20 = max per chunk of 10 zombies.
        count = Mathf.Min(count, (int)((GetMaxZombies() / 6) * (1 - maxCountScalingPercent)));

        //if (log)
        //{
        //    Log.Out("GetPOISpawnCount 33 : " + chunkKey + " - " + count);
        //    Log.Out("GetPOISpawnCount -------------------------------------------------------");
        //}

        return count;
    }

    public static List<BiomeSpawnEntityGroupData> GetBiomeSpawnEntityGroupData(List<BiomeSpawnEntityGroupData> biomeSpawnEntityGroupList, Chunk chunk, out int partyLevel)
    {
        // Get Players gamestage
        partyLevel = GetGamestageFromBounds(chunk.X * 16, chunk.Y * 16, chunk.Z * 16, chunk.Key.ToString());

        // Get list of biomeSpawnEntityGroup
        int matchingGameStage = 0;
        List<BiomeSpawnEntityGroupData> list = new List<BiomeSpawnEntityGroupData>();
        foreach (var x in biomeSpawnEntityGroupList.OrderByDescending(x => x.gameStage).AsEnumerable())
        {
            //Log.Out("SpawnLogic-" + guid + " : " + x.entityGroupRefName + " : " + x.gameStage);
            // Automatically include spawns for animals whatever the GS
            if (x.entityGroupRefName.ToLower().Contains("animal"))
                list.Add(x);

            // Finding matching gamestage
            if (matchingGameStage == 0 && x.gameStage <= partyLevel)
                matchingGameStage = x.gameStage;

            // Animals already included so they are excluded
            if (matchingGameStage > 0 && !x.entityGroupRefName.ToLower().Contains("animal"))
            {
                if (x.gameStage != matchingGameStage) continue;
                list.Add(x);
            }
        }

        return list;
    }

    private static int GetMaxZombies()
    {
        return GamePrefs.GetInt(EnumGamePrefs.MaxSpawnedZombies) > 100 ? 100 : GamePrefs.GetInt(EnumGamePrefs.MaxSpawnedZombies);
    }
}
