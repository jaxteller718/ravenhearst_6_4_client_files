﻿using System;
using System.Linq;
using System.Collections.Generic;
using Harmony;
using UnityEngine;
using System.Reflection;
using DMT;
using System.Reflection.Emit;

public class RH_IncreaseTraderQuests
{
    public class RH_IncreaseTraderQuests_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(EntityNPC))]
    [HarmonyPatch("PopulateActiveQuests")]
    [HarmonyPatch(new Type[] { typeof(EntityPlayer), typeof(int) })]
    public class PatchEntityNPCPopulateActiveQuests
    {
        static bool Prefix(EntityNPC __instance, ref List<Quest> __result, EntityPlayer player, int currentTier, ref List<Vector2> ___usedPOILocations, ref List<int> ___tempTopTierQuests)
        {
            if (__instance.questList == null)
            {
                __instance.PopulateQuestList();
            }
            bool @bool = GameStats.GetBool(EnumGameStats.EnemySpawnMode);
            List<Quest> list = new List<Quest>();
            ___usedPOILocations.Clear();
            if (currentTier == -1)
            {
                currentTier = player.QuestJournal.GetCurrentFactionTier(0, 0, false);
            }

            //currentTier = 3; // TEMP SET TO TIER 5
            //Log.Out("PatchEntityNPCPopulateActiveQuests EntityNPC.questList : " + __instance.questList.Count);

            // Only show Buried Treasure quests for Current Tier and 1 Below this tier.
            var previousTier = currentTier > 1 ? (currentTier - 1) : 1;
            for (int tier = previousTier; tier <= currentTier; tier++)
            {
                var questListPerTier = __instance.questList.Where(quest => quest.QuestClass.DifficultyTier == tier).ToList();
                var tierCount = 0;
                for (int k = 0; k < 200; k++)
                {
                    var index = __instance.rand.RandomRange(questListPerTier.Count);
                    var questEntry = questListPerTier[index];

                    if (currentTier > 2 && questEntry.QuestClass.Objectives.Any(obj => obj is ObjectiveFetchFromTreasure))
                        continue;

                    if (__instance.rand.RandomFloat < questEntry.Prob)
                    {
                        Quest quest = questEntry.QuestClass.CreateQuest();
                        quest.QuestGiverID = __instance.entityId;
                        quest.SetPositionData(Quest.PositionDataTypes.QuestGiver, __instance.position);
                        quest.SetupTags();
                        if (@bool || (quest.QuestTags & QuestTags.clear) == QuestTags.none)
                        {
                            if (quest.SetupPosition(__instance, ___usedPOILocations, player.entityId))
                            {
                                list.Add(quest);
                                tierCount++;
                            }
                            if (tierCount == 5)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            __result = list;

            return false;
        }
    }


    // Reset quests if 5 quests have been completed in a day!
    [HarmonyPatch(typeof(EntityNPC))]
    [HarmonyPatch("OnEntityActivated")]
    [HarmonyPatch(new Type[] { typeof(int), typeof(Vector3i), typeof(EntityAlive) })]
    public class PatchEntityNPCOnEntityActivated
    {
        static bool Prefix(EntityNPC __instance, ref bool __result, int _indexInBlockActivationCommands, Vector3i _tePos, EntityAlive _entityFocusing)
        {
            var activeQuests = QuestEventManager.Current.GetQuestList(GameManager.Instance.World, __instance.entityId, _entityFocusing.entityId);
            var currentTier = (_entityFocusing as EntityPlayerLocal).QuestJournal.GetCurrentFactionTier(0, 0, false);

            //Log.Out("PatchEntityNPCOnEntityActivated activequests : " + (activeQuests == null ? "null" : activeQuests.Count.ToString()));
            //Log.Out("PatchEntityNPCOnEntityActivated currentTier : " + currentTier);

            if (activeQuests != null && activeQuests.Any() && currentTier > 1 && activeQuests.Count == 5)
            {
                //Log.Out("PatchEntityNPCOnEntityActivated Reset Active Quests");
                QuestEventManager.Current.npcQuestData[__instance.entityId].PlayerQuestList[_entityFocusing.entityId].QuestList = new List<Quest>();
            }

            //if (activeQuests == null || !activeQuests.Any())
            //{
            //    if (QuestEventManager.Current.npcQuestData.ContainsKey(__instance.entityId) && QuestEventManager.Current.npcQuestData[__instance.entityId].PlayerQuestList.ContainsKey(_entityFocusing.entityId))
            //    {
            //        var nextUpdateTime = QuestEventManager.Current.npcQuestData[__instance.entityId].PlayerQuestList[_entityFocusing.entityId].LastUpdate + 24000;
            //        var daytimeFormatter = new CachedStringFormatter<int, int, int>((int _day, int _hour, int _min) => string.Format(Localization.Get("xuiDayTimeLong", ""), _day, _hour, _min));
            //        var nextUpdateString = daytimeFormatter.Format(GameUtils.WorldTimeToDays(nextUpdateTime), GameUtils.WorldTimeToHours(nextUpdateTime), GameUtils.WorldTimeToMinutes(nextUpdateTime));
            //        LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(_entityFocusing as EntityPlayerLocal);
            //        GameManager.ShowTooltipWithAlert(uiforPlayer.xui.playerUI.entityPlayer, string.Format(Localization.Get("questsNotAvailableUntil", Localization.QuestPrefix), nextUpdateString), "ui_denied");
            //    }
            //}

            return true;
        }
    }
}
