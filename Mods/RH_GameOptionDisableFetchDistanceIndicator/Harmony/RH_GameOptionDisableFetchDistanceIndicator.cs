﻿using System;
using Harmony;
using UnityEngine;
using System.Reflection;
using DMT;
using System.Collections.Generic;

public class RH_GameOptionDisableFetchDistanceIndicator
{
    public class RH_GameOptionDisableFetchDistanceIndicator_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(ObjectiveFetchFromContainer))]
    [HarmonyPatch("get_StatusText")]
    public class PatchObjectiveFetchFromContainerStatusText
    {
        public static bool Prefix(ObjectiveFetchFromContainer __instance, ref string __result)
        {
            if (!GameStats.GetBool(EnumGameStats.ShowFetchDistanceIndicator))
            {
                __result = "....";
                return false;
            }

            return true;
        }
    }
}
