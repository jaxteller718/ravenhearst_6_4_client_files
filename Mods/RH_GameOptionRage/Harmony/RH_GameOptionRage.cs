﻿using System;
using Harmony;
using UnityEngine;
using System.Reflection;
using DMT;
using System.Collections.Generic;

public class RH_GameOptionRage
{
    public class RH_GameOptionRage_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(EntityZombie))]
    [HarmonyPatch("ProcessDamageResponseLocal")]
    [HarmonyPatch(new Type[] { typeof(DamageResponse) })]
    public class PatchEntityZombieProcessDamageResponseLocal
    {
        static void Postfix(EntityZombie __instance, ref DamageResponse _dmResponse)
        {
            //0 = on
            //1 = off
            if (GameStats.GetInt(EnumGameStats.ZombieRage) != 0)
            {
                return;
            }

            if (!__instance.isEntityRemote)
            {
                int @int = GameStats.GetInt(EnumGameStats.GameDifficulty);
                float num = EntityZombie.rageChances[@int];
                if (_dmResponse.Source.GetDamageType() == EnumDamageTypes.BloodLoss)
                {
                    num *= 0.1f;
                }
                if (__instance.rand.RandomFloat < num)
                {
                    if (__instance.rand.RandomFloat < EntityZombie.superRageChances[@int])
                    {
                        __instance.moveSpeedRagePer = 2f;
                        __instance.moveSpeedScaleTime = 30f;
                        __instance.PlayOneShot(__instance.GetSoundAlert(), false);
                        return;
                    }
                    __instance.moveSpeedRagePer = 0.5f + __instance.rand.RandomFloat * 0.5f;
                    __instance.moveSpeedScaleTime = 4f + __instance.rand.RandomFloat * 6f;
                }
            }
        }
    }
}
